<?php
include "validate.php";

$salt = 'XyZzy12*_';
$hash = hash('md5',$salt.'php123');

$message = "";
$email = "";

if(isset($_POST['who']) && isset($_POST['pass'])){
	if(!empty($_POST['who']) && !empty($_POST['pass'])){
		$check = hash('md5', $salt.$_POST['pass']);
		if(check_email($_POST['who']) && $check == $hash){
		 $email = input_check($_POST['who']);

         // Redirect the browser to autos.php
		 header("Location: autos.php?name=".urlencode($email));
		 error_log("Login success ".$_POST['who']);
         return;
		}
		else{
			if(!check_email($_POST['who'])) {$message = "Email must have an at-sign (@)";}
			if($check!=$hash){$message = "Incorrect password!";}
			error_log("Login fail ".$_POST['who']." $check");
		}  
	}

	else{
		$message = "Email and password are required";
  
  }
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Janta Roy Antor</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<h4>Please Log In</h4>
	<span class="text text-danger"><?php echo $message; ?></span>
	<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
		<label for="who">Email:</label> <br>	
		<input type="text" name="who" > <br>

		<label for="pass">Password:</label> <br>
		<input type="password" name="pass"> <br> <br><!-- password: php123 -->

		<input type="submit" name="Login" value="Log In">
	</form>

</div>	
</body>
</html>