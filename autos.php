<?php
if(isset($_POST['logout'])){
	header("Location: index.php");
	return;
}

require_once 'pdo.php';
include "validate.php";


if(empty($_GET['name'])){
	die('Name Parameter missing');
}


$message = "";
$make = "";
$year = "";
$mileage = "";
$success = "";

if(isset($_POST['save'])){

    if(!empty($_POST['make']) && !empty($_POST['year']) && !empty($_POST['mileage'])){

    	$make = input_check($_POST['make']);

    	check_number($_POST['year']) ? $year = input_check($_POST['year']) : $message = "Mileage and year must be numeric";
    	check_number($_POST['mileage']) ? $mileage = input_check($_POST['mileage']) : $message = "Mileage and year must be numeric";
    }
    else{

    	if(empty($_POST['make'])) { $message = "Make is required!";}
    	if(empty($_POST['year'])) { $message = "Year is required!";}
    	if(empty($_POST['mileage'])) {$message = "Mileage is required!";}
    	if(empty($_POST['make']) && empty($_POST['year']) && empty($_POST['mileage'])){$message = "Empty form fields!";}
    }

    if(!empty($make) && !empty($year) && !empty($mileage)){
    	$stmt = $pdo->prepare('INSERT INTO autos (make,year,mileage) VALUES (:mk,:yr,:ml)');
	    $stmt->execute(array(
		':mk' => $make,
		':yr' => $year,
		':ml' => $mileage
	));
	    $success = "Record Inserted";
    }
}

$stmt2 = $pdo->prepare('SELECT * FROM autos ORDER BY auto_id DESC');
$stmt2->execute();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Janta Roy Antor</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<h4>Add Auto Information for <?php echo $_GET['name'];  ?>  </h4>
	<span class="text text-danger"><?php echo $message; ?></span>
	<span class="text text-success"><?php echo $success; ?></span>
	<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']).'?name='.$_GET['name']; ?>" method="POST">
		<label for="make">Make:</label> <br>	
		<input type="text" name="make"> <br>

		<label for="year">Year:</label> <br>
		<input type="text" name="year"> <br>

		<label for="mileage">Mileage:</label> <br>
		<input type="text" name="mileage"> <br> <br>

		<input type="submit" name="save" value="Add">
	</form>

<form method="POST" action="">
	<input type="submit" name="logout" value="Logout">
</form>
	<h4>Autos</h4>
	<hr>
	<table class="table table-dark">
		
		<tr>
			<th>Make</th>
			<th>Year</th>
			<th>Mileage</th>
		</tr>
		<?php while  ($row = $stmt2->fetch(PDO::FETCH_ASSOC)) { ?>
		<tr>
			<td><?php echo $row['make'];?></td>
			<td><?php echo $row['year'];?> </td>
			<td> <?php echo $row['mileage'];?> </td>
		</tr>
		<?php } ?>
	</table>
</div>	
</body>
</html>